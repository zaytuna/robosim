package simulation;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import fakes.DriverStation;
import fakes.IterativeRobot;

class SimulationWindow extends JFrame {
	private static final long serialVersionUID = -2781679292328438791L;

	public static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

	IterativeRobot software;
	DriverStation station = DriverStation.getInstance();
	RobotDesign hardware = new RobotDesign();

	SimulationWindow(String robotName) {
		super("RoboSim");

		try {
			software = (IterativeRobot) (Class.forName(robotName).getDeclaredConstructor().newInstance());
		} catch (Exception e) {
			software = new IterativeRobot();
			System.out.println("Reflective constructor failed miserably!");
		}

		setBounds(0, 0, screen.width, screen.height);
		setUndecorated(true);
	}

	public static void centerFrame(int frameWidth, int frameHeight, Component c) {
		Toolkit tools = Toolkit.getDefaultToolkit();
		Dimension screen = tools.getScreenSize();
		int xUpperLeftCorner = (screen.width - frameWidth) / 2;
		int yUpperLeftCorner = (screen.height - frameHeight) / 2;

		c.setBounds(xUpperLeftCorner, yUpperLeftCorner, frameWidth, frameHeight);
	}

	public static void main(String[] args) {
		if (args.length == 0)
			args = new String[] { "IterativeRobot" };
		SimulationWindow sw = new SimulationWindow(args[0]);
		sw.setVisible(true);
	}
}