package upperlevel;

import java.awt.*;
import javax.swing.*;

class ProgressButton extends JButton {
	private static final long serialVersionUID = -6939589668073391810L;

	double done = 0;// from 0 to 1
	Color normal;
	Color selected = new Color(0, 0, 0, 100);
	String text;
	Icon icon;

	ProgressButton(String text) {
		setDoubleBuffered(true);
		this.text = text;
	}
	
	ProgressButton(String text, Icon i) {
		setDoubleBuffered(true);
		this.text = text;
		this.icon = i;
	}
	
	public void update(double d) {
		done = d % 1;
	}
	
	public void update() {
		done += .01;
		done %= 1;
	}
	
	public void setBackground(Color c) {
		this.normal = new Color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
	}
	
	public void setProgressColor(Color c) {
		selected = new Color(c.getRed(), c.getGreen(), c.getBlue(), 100);
	}
	
	public void setProgressColor(Color c, boolean b) {
		selected = c;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(selected);
		g.fillRect(0, 0, (int) (getWidth() * done), getHeight());
		g.setColor(normal);
		g.fillRect((int) (getWidth() * done), 0, getWidth() - (int) (getWidth() * done), getHeight());

		if (icon != null)
			icon.paintIcon(this, g, (getWidth() - icon.getIconWidth()) / 2, (getHeight() - icon.getIconHeight()) / 2);

		FontMetrics fm = g.getFontMetrics(getFont());
		java.awt.geom.Rectangle2D rect = fm.getStringBounds(text, g);

		int textHeight = (int) (rect.getHeight());
		int textWidth = (int) (rect.getWidth());
		int panelHeight = this.getHeight();
		int panelWidth = this.getWidth();

		// Center text horizontally and vertically
		int x = (panelWidth - textWidth) / 2;
		int y = (panelHeight - textHeight) / 2 + fm.getAscent();

		g.setColor(getForeground());
		g.drawString(text, x, y); // Draw the string.
	}
}