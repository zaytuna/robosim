package upperlevel;
import java.io.*;
import java.util.*;

class ImportData implements Runnable
{
	public static String[] simulationFiles = {"SimulationWindow.java","Jaguar.java","IterativeRobot.java","Joystick.java",
		"Accelerometer.java","GenericHID.java","Gyro.java","Timer.java","Solenoid.java","DriverStation.java",
		"Watchdog.java","RobotBase.java","RobotDesign.java","Vector.java","config.txt"};
	public static String javacDir = "C:\\Program Files\\Java\\jdk1.6.0_16\\bin";

	Process process;
	String name;

	private ImportData(Process p, String n)
	{
		this.process = p;
		this.name = n;
	}

	public static void integrateIntoProgram(File dir,String programName, String mainClassName)
	{
		if(dir.isDirectory())
		{
			File dirTo = new File("Builds\\"+programName+"\\");
			if(dirTo.exists())
				dirTo.delete();

			dirTo.mkdir();


			for(String s: dir.list())
			{
				System.out.print("\n"+s+"... ");
				if(s.endsWith(".java"))
				{
					System.out.print("copying... ");
					String[] replace = getFileContents(new File("SearchTable.txt")).split("\n");

					try
					{
						File toFile = new File(dirTo.getAbsolutePath(),s);
						if(!toFile.exists())
							toFile.createNewFile();
						BufferedReader in = new BufferedReader(new FileReader(new File(dir,s)));
						BufferedWriter out = new BufferedWriter(new FileWriter(toFile,false));

						while(true)
						{
							String temp = in.readLine();

							if(temp==null)
								break;

							if(temp.startsWith("import"))
								continue;
							for(String str:replace)
							{
								String[] parts = str.split("\t");
								temp = temp.replaceAll(parts[0],parts[1]);
							}

							out.write(temp);
							out.flush();
						}
						in.close();
						out.close();
					}
					catch(Exception e)
					{
						e.printStackTrace();
						//System.out.println("Error in ImportData: IO section @integrateIntoProgram:\n"+e);
					}
					System.out.println("done");
				}
			}

			for(String s:simulationFiles)
			{
				copy(new File("Simulation Files\\"+s),new File("Builds\\"+programName+"\\"+s));
			}

			try
			{
				BufferedWriter out = new BufferedWriter(new FileWriter(new File(dirTo,"config.txt")));
				out.write(mainClassName);
				out.close();
			}
			catch(Exception e){e.printStackTrace();}
		}
	}
	public static void copy(File from, File to)
	{
		try
		{
			if(!from.exists())
			{
				System.out.println("\tSource not found: "+from.getAbsolutePath());
				return;
			}

			if(!to.exists())
				to.createNewFile();
			BufferedReader in = new BufferedReader(new FileReader(from));
			BufferedWriter out = new BufferedWriter(new FileWriter(to,false));

			while(true)
			{
				String temp = in.readLine();

				if(temp==null)
					break;

				out.write(temp+"\n");
				out.flush();
			}
			in.close();
			out.close();
		}
		catch(Exception e)
		{
			System.out.println("Error in ImportData: IO section @copy(File a, File b):\n"+e);
		}
	}
	public static String getFileContents(File f)
	{
		try
		{
			StringBuilder sb = new StringBuilder();

			BufferedReader in = new BufferedReader(new FileReader(f));
			while(true)
			{
				String temp = in.readLine();
				if(temp == null)
					break;
				sb.append(temp+"\n");
			}
			in.close();
			return sb.toString();
		}
		catch(Exception e){e.printStackTrace();}
		return null;
	}
	public static void runCode(File dir)
	{
		try
		{
			String mainClassName = getFileContents(new File(dir,"config.txt")).split("\n")[0];

			follow(Runtime.getRuntime().exec(javacDir+"\\javac -g "+dir.getAbsolutePath()+"\\SimulationWindow.java"),
				"COMPILE_PROCESS");

			new Thread(new ImportData(Runtime.getRuntime().exec("java "+dir.getAbsolutePath()+"\\SimulationWindow",
				new String[]{mainClassName}),"RUN_PROCESS")).start();
		}
		catch(Exception e){e.printStackTrace();}
	}
	public static void follow(Process p,String name)
	{
		Scanner sc = new Scanner(p.getInputStream());

		while(sc.hasNext())
		{
			String nxt = sc.next();
			System.out.print(nxt.replaceAll("\n","\n<"+name+">"));
		}

		try
		{
			p.waitFor();
		}
		catch(Exception e){e.printStackTrace();}
		sc.close();
		System.out.println("DONE WITH "+name);
	}
	public void run()
	{
		follow(process,name);
	}
}