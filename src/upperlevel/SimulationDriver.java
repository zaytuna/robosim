package upperlevel;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/*
 Not the simulation window, but the simulation driver. Imports code.
 */
class SimulationDriver extends JFrame implements ActionListener, Runnable, PropertyChangeListener {
	private static final long serialVersionUID = 8303390508189112239L;

	public static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	private final static int WIDTH = screen.width / 3;

	double fractionDone = 0;
	double to = 0;
	int toWidth = WIDTH;
	int actWidth = WIDTH;

	// Components
	ProgressButton importNew = new ProgressButton("Import Code");
	ProgressButton changeRobot = new ProgressButton("Change Robot");
	ProgressButton run = new ProgressButton("Run Simulation");
	JButton exit = new JButton("X");

	// Components for run:
	JList<String> list = new JList<String>();// hidden at fist, but slides up from bottom
	JScrollPane pane = new JScrollPane(list);
	JButton start = new JButton("Go!");
	JComboBox<String> mainClassName = new JComboBox<String>();
	JLabel mainClassL = new JLabel("Main class: ", JLabel.CENTER);

	// Components for importNew
	JFileChooser fileChooser = new JFileChooser();
	ProgressButton install = new ProgressButton("Integrate", new ImageIcon("check.png"));
	JButton options = new JButton("Options");

	SimulationDriver() {
		setLayout(null);
		setBounds(WIDTH, 50, WIDTH, screen.height - 100);

		importNew.setBounds(0, 0, WIDTH, (screen.height - 100) / 3);
		changeRobot.setBounds(0, (screen.height - 100) / 3, WIDTH, (screen.height - 100) / 3);
		run.setBounds(0, (2 * (screen.height - 100)) / 3, WIDTH, (screen.height - 100) / 3);
		exit.setBounds(WIDTH - 45, 0, 45, 30);

		pane.setBounds(0, (screen.height - 100), WIDTH, 0);
		fileChooser.setBounds(0, (screen.height - 100) / 3, WIDTH, 0);
		install.setBounds(0, (screen.height - 100) / 3, WIDTH, 0);
		mainClassName.setBounds(0, (screen.height - 100) / 3, WIDTH, 0);
		mainClassL.setBounds(0, (screen.height - 100) / 3, WIDTH, 0);

		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		add(run);
		add(importNew);
		add(changeRobot);
		add(pane);
		add(start);
		add(fileChooser);
		add(install);
		add(exit);
		add(mainClassName);
		add(mainClassL);

		Font font = new Font("SERIF", Font.BOLD, 40);

		getContentPane().setComponentZOrder(exit, 0);
		run.setFont(font);
		importNew.setFont(font);
		changeRobot.setFont(font);
		start.setFont(font);
		install.setFont(font);

		run.setFocusPainted(false);
		changeRobot.setFocusPainted(false);
		importNew.setFocusPainted(false);
		start.setFocusPainted(false);
		exit.setFocusPainted(false);
		install.setFocusPainted(false);
		options.setFocusPainted(false);

		importNew.addActionListener(this);
		run.addActionListener(this);
		changeRobot.addActionListener(this);
		start.addActionListener(this);
		exit.addActionListener(this);
		options.addActionListener(this);
		install.addActionListener(this);

		importNew.setBackground(new Color(24, 40, 40));
		importNew.setForeground(Color.WHITE);
		install.setForeground(Color.WHITE);
		changeRobot.setBackground(new Color(96, 112, 45));
		install.setBackground(new Color(49, 27, 0));
		// changeRobot.setForeground(Color.WHITE);
		run.setBackground(new Color(230, 250, 135));
		// importNew.setProgressColor(Color.GREEN,true);
		run.setProgressColor(new Color(150, 0, 0, 200), true);
		exit.setBackground(Color.RED);
		start.setBackground(Color.ORANGE);

		list.setFont(new Font("Sans_SERIF", Font.ITALIC, 20));

		fileChooser.setControlButtonsAreShown(false);
		fileChooser.setOpaque(true);
		fileChooser.addPropertyChangeListener(this);

		setUndecorated(true);

		MotionListener.makeMovable(this, importNew, changeRobot, run);

		new Thread(this).start();
	}

	public void propertyChange(PropertyChangeEvent e) {
		java.io.File f = fileChooser.getSelectedFile();

		if (f == null)
			return;

		if (f.isDirectory()) {
			mainClassName.removeAllItems();
			for (String s : f.list())
				mainClassName.addItem(s);
		}
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == changeRobot) {
			toWidth = (toWidth == WIDTH) ? 2 * WIDTH : WIDTH;
			to = 0;
		} else
			toWidth = WIDTH;

		if (evt.getSource() == importNew) {
			to = (to == -1) ? 0 : -1;
		} else if (evt.getSource() == run) {
			list.setListData(new java.io.File("Builds\\").list());
			to = (to == 1) ? 0 : 1;
		} else if (evt.getSource() == start) {
			// RUN SIMULATION
			if (list.getSelectedValue() == null) {
				JOptionPane.showMessageDialog(null, "Select a program");
				return;
			}
			ImportData.runCode(new java.io.File("Builds\\", (String) (list.getSelectedValue())));
		} else if (evt.getSource() == install) {
			// INSTALL PROGRAM from directory
			ImportData.integrateIntoProgram(fileChooser.getSelectedFile(), fileChooser.getSelectedFile().getName(),
					(String) (mainClassName.getSelectedItem()));
		} else if (evt.getSource() == options) {
			// Bring up options window
		} else if (evt.getSource() == exit) {
			System.exit(0);
		}
		applyWidth();
	}

	public void applyWidth() {
		setBounds(getX() + (getWidth() - toWidth) / 2, 50, toWidth, screen.height - 100);
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(10);
			} catch (Exception e) {
			}

			if ((fractionDone >= -.01) && (to == 1) && !(Math.abs(pane.getHeight() - (screen.height - 100) / 2) < 1)
					|| ((fractionDone >= -.01) && !(Math.abs(fractionDone) < .005))) {
				importNew.setSize(WIDTH, (int) ((getHeight() / 3) - (fractionDone * getHeight() / 6)));

				changeRobot.setBounds(0, importNew.getHeight(), WIDTH, (int) ((getHeight() / 3) - (fractionDone
						* getHeight() / 6)));

				run.setBounds(0, changeRobot.getHeight() + changeRobot.getY(), WIDTH,
						(int) ((getHeight() / 3) - (fractionDone * getHeight() / 6)));

				pane.setBounds(0, (int) (getHeight() - (fractionDone * getHeight() / 2)), WIDTH * 2 / 3,
						(int) (fractionDone * getHeight() / 2));

				start.setBounds(WIDTH * 2 / 3, (int) (getHeight() - (fractionDone * getHeight() / 2)), WIDTH / 3,
						(int) (fractionDone * getHeight() / 2));

				pane.revalidate();

				fractionDone += (to - fractionDone) / 12;
				run.update(fractionDone);
			}
			if ((fractionDone <= .01) && (to == -1)
					&& !(Math.abs(fileChooser.getHeight() - (screen.height - 100) / 2) < 1)
					|| ((fractionDone <= .01) && !(Math.abs(fractionDone) < .005))) {
				importNew.setSize(WIDTH, (int) ((getHeight() / 3) - (Math.abs(fractionDone) * getHeight() / 6)));

				changeRobot.setBounds(0, importNew.getHeight() + (int) (Math.abs(fractionDone) * getHeight() / 2),
						WIDTH, (int) ((getHeight() / 3) - (Math.abs(fractionDone) * getHeight() / 6)));

				run.setBounds(0, changeRobot.getHeight() + changeRobot.getY(), WIDTH, (int) ((getHeight() / 3) - (Math
						.abs(fractionDone)
						* getHeight() / 6)));

				fileChooser.setBounds(0, (int) (importNew.getHeight()), WIDTH, (int) (Math.abs(fractionDone)
						* getHeight() / 3));
				install.setBounds(WIDTH / 3, (int) (fileChooser.getHeight() + fileChooser.getY()), 2 * WIDTH / 3,
						(int) (Math.abs(fractionDone) * getHeight() / 6) - 30);
				mainClassName.setBounds(5 * WIDTH / 9, changeRobot.getY() - 28, 4 * WIDTH / 9 - 5, 25);
				mainClassL.setBounds(WIDTH / 3, changeRobot.getY() - 30, 2 * WIDTH / 9, 30);

				fileChooser.revalidate();

				fractionDone += (to - fractionDone) / 12;
				importNew.update(Math.abs(fractionDone));
			}
			if (actWidth != toWidth) {
				actWidth += (toWidth - actWidth) / 10 - 1;
				exit.setLocation(actWidth - 45, 0);
			}
			repaint();
		}
	}

	public void paint(Graphics g) {
		if (getBufferStrategy() == null)
			createBufferStrategy(2);
		g = getBufferStrategy().getDrawGraphics();
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setStroke(new BasicStroke(5));
		g.drawLine(0, 0, getWidth(), 0);
		g.drawLine(0, 0, 0, getHeight());
		g.drawLine(getWidth(), 0, getWidth(), getHeight());
		g.drawLine(0, getHeight(), getWidth(), getHeight());
		getBufferStrategy().show();
	}

	public static void main(String[] args) {
		SimulationDriver sd = new SimulationDriver();
		sd.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		sd.setVisible(true);
	}

	public static class MotionListener extends MouseAdapter implements MouseMotionListener, MouseListener {
		Component owner;
		Point pressedAt = null;

		public MotionListener(Component c) {
			owner = c;
		}
		public static void makeMovable(Component c, Component... cs) {
			MotionListener m = new MotionListener(c);
			c.addMouseListener(m);
			c.addMouseMotionListener(m);

			for (Component comp : cs) {
				comp.addMouseListener(m);
				comp.addMouseMotionListener(m);
			}
		}
		public void mousePressed(MouseEvent evt) {
			pressedAt = evt.getPoint();
		}
		public void mouseReleased(MouseEvent evt) {
			owner.setBounds(owner.getX() - pressedAt.x + evt.getX(), owner.getY() - pressedAt.y + evt.getY(), owner
					.getWidth(), owner.getHeight());
		}
		public void mouseDragged(MouseEvent evt) {
			owner.setBounds(owner.getX() - pressedAt.x + evt.getX(), owner.getY() - pressedAt.y + evt.getY(), owner
					.getWidth(), owner.getHeight());
		}
	}
}