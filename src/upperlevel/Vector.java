package upperlevel;
class Vector
{
	double x, y;

	Vector(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
	public double magnitude()
	{
		return Math.sqrt(x*x+y*y);
	}
	public double getHorizontalAngle()
	{
		return Math.atan2(y,x);
	}
	public double dotProduct(Vector other)
	{
		return x*other.x+y*other.y;
	}
	public double angleBetween(Vector other)
	{
		return Math.acos(dotProduct(other)/(magnitude()*other.magnitude()));
	}
}