package fakes;

public class Watchdog implements Runnable {
	private static Watchdog m_instance;
	boolean enabled, alive, immortality;
	/**
	 * Default expiration for the watchdog in seconds
	 */
	public double expiration = .5;
	double energy = 1;// when it gets to 0, the dog is finished.

	/**
	 * The Watchdog is born.
	 */
	protected Watchdog() {
		setEnabled(true);
	}

	public static Watchdog getInstance() {
		if (m_instance == null)
			m_instance = new Watchdog();

		return m_instance;
	}

	public void feed() {
		energy = 1;
	}

	public void kill() {
		alive = false;
	}

	public double getExpiration() {
		return expiration;
	}

	public void setExpiration(double expiration) {
		this.expiration = expiration;
	}

	public boolean getEnabled() {
		return !immortality;
	}

	public void setEnabled(final boolean enabled) {
		immortality = !enabled;
	}

	public boolean isAlive() {
		return alive;
	}

	boolean isSystemActive() {
		return alive;
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(50);
				energy -= .05 / expiration;
				if (energy <= 0)
					kill();
			} catch (Exception e) {
			}
		}
	}
}