package fakes;

import javax.swing.*;
import java.util.*;
import java.awt.*;

public class DriverStation extends JPanel {
	private static final long serialVersionUID = -3193124800318724541L;

	private static DriverStation driver = new DriverStation();
	private ArrayList<Joystick> joysticks = new ArrayList<Joystick>();

	int mode = 0; // 0 is autonomous, 1 is teleoperated,

	private DriverStation() {
		super(new FlowLayout(FlowLayout.CENTER));
	}

	public void addJoystick(Joystick js) {
		joysticks.add(js);
		add(js.getComponent());
	}

	public static DriverStation getInstance() {
		return driver;
	}

	public boolean isAutonomous() {
		return mode == 0;
	}

	public boolean isOperatorControl() {
		return mode == 1;
	}
}