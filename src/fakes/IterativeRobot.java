package fakes;
public class IterativeRobot extends RobotBase
{
	public void teleopContinuous(){}
	public void robotInit(){}
	public void disabledInit(){}
	public void autonomousInit(){}
	public void teleopInit(){}
	public void disabledPeriodic(){}
	public void autonomousPeriodic(){}
	public void teleopPeriodic(){}
	public void autonomousContinuous(){}
	public void disabledContinuous(){}
	@Override
	public void startCompetition() {
	}
}