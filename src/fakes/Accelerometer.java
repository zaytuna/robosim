package fakes;

public class Accelerometer extends SensorBase {
	double acceleration;
	int port, slot;

	public Accelerometer(final int channel) {
	}

	public Accelerometer(final int slot, final int channel) {
	}

	protected void free() {
	}

	public double getAcceleration() {
		return acceleration;
	}

	public double pidGet() {
		return getAcceleration();
	}

	public void setSensitivity(double s) {
	}

	public void setZero(double voltage) {
	}

	@Override
	public int getPort() {
		return port;
	}

	@Override
	public int getSlot() {
		return slot;
	}

	@Override
	public double getValue() {
		return acceleration;
	}
}