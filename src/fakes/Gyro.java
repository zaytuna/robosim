package fakes;

public class Gyro extends SensorBase {
	double angle, m_voltsPerDegreeSecond;
	int slot, port;

	public Gyro(int slot, int channel) {
	}

	public Gyro(int channel) {
	}

	public void reset() {
	}

	protected void free() {
	}

	public double getAngle() {
		return angle;
	}

	public void setSensitivity(double voltsPerDegreePerSecond) {
		m_voltsPerDegreeSecond = voltsPerDegreePerSecond;
	}

	public double pidGet() {
		return getAngle();
	}

	public double getValue() {
		return getAngle();
	}

	@Override
	public int getPort() {
		return port;
	}

	@Override
	public int getSlot() {
		return slot;
	}
}