package fakes;

public abstract class SensorBase {
	public abstract double getValue();
	public abstract int getSlot();
	public abstract int getPort();

	protected void free() {
	}
}