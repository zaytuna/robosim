package fakes;

/*
 * The Joystick class to mimick that of the robot library.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Joystick extends GenericHID {
	static final byte kDefaultXAxis = 1;
	static final byte kDefaultYAxis = 2;
	static final byte kDefaultZAxis = 3;
	static final byte kDefaultTwistAxis = 3;
	static final byte kDefaultThrottleAxis = 4;
	static final int kDefaultTriggerButton = 1;
	static final int kDefaultTopButton = 2;

	JoystickPanel component = new JoystickPanel(Color.BLACK);
	private final int port;
	private final byte[] axes;// 1 = x, 2 = y, 3 = twist, 4 = throttle, 5 = x
	// (miniJoystick), 6 = y (miniJoystick)
	private final byte[] buttons;

	/**
	 * Represents an analog axis on a joystick.
	 */
	public static class AxisType {
		/**
		 * The integer value representing this enumeration
		 */
		public final int value;
		static final int kX_val = 0;
		static final int kY_val = 1;
		static final int kZ_val = 2;
		static final int kTwist_val = 3;
		static final int kThrottle_val = 4;
		static final int kNumAxis_val = 5;
		/**
		 * axis: x-axis
		 */
		public static final AxisType kX = new AxisType(kX_val);
		/**
		 * axis: y-axis
		 */
		public static final AxisType kY = new AxisType(kY_val);
		/**
		 * axis: z-axis
		 */
		public static final AxisType kZ = new AxisType(kZ_val);
		/**
		 * axis: twist
		 */
		public static final AxisType kTwist = new AxisType(kTwist_val);
		/**
		 * axis: throttle
		 */
		public static final AxisType kThrottle = new AxisType(kThrottle_val);
		/**
		 * axis: number of axis
		 */
		public static final AxisType kNumAxis = new AxisType(kNumAxis_val);

		private AxisType(int value) {
			this.value = value;
		}
	}

	/**
	 * Represents a digital button on the JoyStick
	 */
	public static class ButtonType {
		/**
		 * The integer value representing this enumeration
		 */
		public final int value;
		static final int kTrigger_val = 0;
		static final int kTop_val = 1;
		static final int kNumButton_val = 2;
		/**
		 * button: trigger
		 */
		public static final ButtonType kTrigger = new ButtonType((kTrigger_val));
		/**
		 * button: top button
		 */
		public static final ButtonType kTop = new ButtonType(kTop_val);
		/**
		 * button: num button types
		 */
		public static final ButtonType kNumButton = new ButtonType((kNumButton_val));

		private ButtonType(int value) {
			this.value = value;
		}
	}

	public Joystick(int port) {
		this.port = port;
		axes = new byte[AxisType.kNumAxis.value];
		buttons = new byte[ButtonType.kNumButton.value];

		axes[AxisType.kX.value] = kDefaultXAxis;
		axes[AxisType.kY.value] = kDefaultYAxis;
		axes[AxisType.kZ.value] = kDefaultZAxis;
		axes[AxisType.kTwist.value] = kDefaultTwistAxis;
		axes[AxisType.kThrottle.value] = kDefaultThrottleAxis;

		buttons[ButtonType.kTrigger.value] = 0;
		buttons[ButtonType.kTop.value] = 0;
	}
	public Component getComponent() {
		return component;
	}
	public double getX(Hand h) {
		return getRawAxis(axes[AxisType.kX.value]);
	}
	public double getY(Hand h) {
		return getRawAxis(axes[AxisType.kY.value]);
	}
	public double getZ(Hand h) {
		return getRawAxis(axes[AxisType.kZ.value]);
	}
	public double getThrottle() {
		return getRawAxis(axes[AxisType.kThrottle.value]);
	}
	public double getTwist() {
		return getRawAxis(axes[AxisType.kTwist.value]);
	}
	public double getRawAxis(int axis) {
		return this.axes[axis];
	}
	public boolean getTrigger(Hand h) {
		return getRawButton(buttons[ButtonType.kTrigger.value]);
	}
	public boolean getTop(Hand h) {
		return getRawButton(buttons[ButtonType.kTop.value]);
	}
	public boolean getBumper(Hand h) {
		return false;
	}
	public boolean getRawButton(final int button) {
		return buttons[button] == 1;
	}
	public boolean getButton(ButtonType button) {
		switch (button.value) {
		case ButtonType.kTrigger_val:
			return getTrigger();
		case ButtonType.kTop_val:
			return getTop();
		default:
			return false;
		}
	}
	public double getMagnitude() {
		return Math.sqrt(Math.pow(getX(), 2) + Math.pow(getY(), 2));
	}
	public double getDirectionRadians() {
		return Math.atan2(getX(), -getY());
	}
	public double getDirectionDegrees() {
		return Math.toDegrees(getDirectionRadians());
	}
	public double getAxis(final AxisType axis) {
		switch (axis.value) {
		case AxisType.kX_val:
			return getX();
		case AxisType.kY_val:
			return getY();
		case AxisType.kZ_val:
			return getZ();
		case AxisType.kTwist_val:
			return getTwist();
		case AxisType.kThrottle_val:
			return getThrottle();
		default:
			return 0.0;
		}
	}

	public int getPort() {
		return port;
	}

	public class JoystickPanel extends JPanel implements MouseListener, MouseMotionListener {
		private static final long serialVersionUID = -4551756482670405261L;

		JoystickPanel(Color c) {
			super(true);
			setBackground(c);
			addMouseListener(this);
		}
		public void paint(Graphics g) {
			super.paint(g);

			Graphics2D g2d = (Graphics2D) g;
			g2d.setColor(Color.BLUE);
			g2d.setStroke(new BasicStroke(10));
			g2d.drawLine(getWidth() / 2, getHeight() / 2, axes[0], axes[1]);
		}
		public void mousePressed(MouseEvent evt) {
		}
		public void mouseReleased(MouseEvent evt) {
		}
		public void mouseClicked(MouseEvent evt) {
		}
		public void mouseDragged(MouseEvent evt) {
		}
		public void mouseEntered(MouseEvent evt) {
		}
		public void mouseMoved(MouseEvent evt) {
		}
		public void mouseExited(MouseEvent evt) {
		}
	}
}