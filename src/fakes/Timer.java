package fakes;

public class Timer implements Runnable {
	double time = 0;
	long start = 0;
	Thread timer = new Thread(this);
	boolean stop = false;

	Timer() {
	}

	public static void delay(double seconds) {
		try {
			Thread.sleep((int) (seconds * 1000));
		} catch (Exception e) {
		}
	}

	public double get() {
		return time;
	}

	public static long getUsClock() {
		return System.nanoTime() / 1000;
	}

	public void reset() {
		time = 0;
	}

	public void start() {
		stop = false;
		timer.start();
	}

	public void stop() {
		stop = true;
	}

	public void run() {
		while (!stop) {
			try {
				Thread.sleep(10);
				time += .01;
			} catch (Exception e) {
			}
		}
	}
}