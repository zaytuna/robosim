package fakes;

public abstract class RobotBase
{
    private final DriverStation m_ds;
    private final Watchdog m_watchdog = Watchdog.getInstance();

	/**
	* Constructor for a generic robot program.
	* User code should be placed in the constuctor that runs before the Autonomous or Operator
	* Control period starts. The constructor will run to completion before Autonomous is entered.
	*
	* This must be used to ensure that the communications code starts. In the future it would be
	* nice to put this code into it's own task that loads on boot so ensure that it runs.
	*/
	protected RobotBase()
	{
		m_ds = DriverStation.getInstance();
	}
	/**
	* Check on the overall status of the system.
	*
	* @return Is the system active (i.e. PWM motor outputs, etc. enabled)?
	*/
	public boolean isSystemActive()
	{
		return m_watchdog.isSystemActive();
	}
	public Watchdog getWatchdog()
	{
		return m_watchdog;
	}

    /**
     * Determine if the Robot is currently disabled.
     * @return True if the Robot is currently disabled by the field controls.
     */
	public boolean isDisabled()
	{
		return !m_ds.isEnabled();
	}

	public boolean isEnabled()
	{
		return m_ds.isEnabled();
	}

	/**
	* Determine if the robot is currently in Autnomous mode.
	* @return True if the robot is currently operating Autonomously as determined by the field controls.
	*/
	public boolean isAutonomous()
	{
		return m_ds.isAutonomous();
	}

    /**
     * Determine if the robot is currently in Operator Control mode.
     * @return True if the robot is currently operating in Tele-Op mode as determined by the field controls.
     */
	public boolean isOperatorControl()
	{
		return m_ds.isOperatorControl();
	}
	public abstract void startCompetition();
}